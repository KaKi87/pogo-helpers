import {
    exists
} from 'https://deno.land/std@0.194.0/fs/mod.ts';

const addCorsHeaders = response => {
    response.headers.append('access-control-allow-origin', '*');
    response.headers.append('access-control-allow-headers', '*');
    response.headers.append('access-control-allow-methods', '*');
};

/**
 * @param server
 * @param {boolean} [isCors]
 * @param {function} [onError]
 * @param {function} [onParseError]
 */
export const createServerHelper = (
    server,
    {
        isCors,
        onError: onServerError,
        onParseError: onServerParseError
    } = {}
) => {
    return {
        /**
         * @param {string} method
         * @param {string} path
         * @param [schema]
         * @param {function} handler
         * @param {function} [onError]
         * @param {function} [onParseError]
         */
        createRoute: ({
            method,
            path,
            schema,
            handler,
            onError: onRouteError,
            onParseError: onRouteParseError
        }) => {
            if(isCors && !server.router.routes.list.find(route => route.method === 'OPTIONS' && route.path === path)){
                server.route({
                    method: 'OPTIONS',
                    path,
                    'handler': (request, h) => {
                        const response = h.response();
                        addCorsHeaders(response);
                        return response;
                    }
                });
            }
            server.route({
                method,
                path,
                handler: async (request, h) => {
                    const
                        timestamp = Date.now(),
                        authorizationHeader = request.headers.get('authorization'),
                        [username, password] = authorizationHeader?.startsWith('Basic ') ? atob(authorizationHeader.slice(6)).split(':') : [],
                        response = h.response();
                    addCorsHeaders(response);
                    let path, params, textBody, body;
                    path = request.params;
                    params = Object.fromEntries(request.searchParams.entries());
                    try {
                        textBody = request.raw.text
                            ? await request.raw.text()
                            : new TextDecoder().decode(await Deno.readAll(request.body));
                        body = JSON.parse(textBody);
                    }
                    catch {}
                    if(schema){
                        const {
                            error: {
                                message: parseError
                            } = {},
                            value: {
                                path: parsedPath,
                                params: parsedParams,
                                body: parsedBody
                            } = {}
                        } = schema.validate({
                            path,
                            params,
                            ...body ? { body } : {}
                        });
                        if(parseError){
                            let isCustomResponse;
                            if(typeof onRouteParseError === 'function')
                                isCustomResponse = onRouteParseError(parseError, response);
                            else if(typeof onServerParseError === 'function')
                                isCustomResponse = onServerParseError(parseError, response);
                            if(!isCustomResponse){
                                response.body = { parseError };
                                response.code(400);
                            }
                            return response;
                        }
                        else {
                            path = parsedPath;
                            params = parsedParams;
                            body = parsedBody;
                        }
                    }
                    try {
                        return await handler(
                            request,
                            h,
                            {
                                timestamp,
                                username,
                                password,
                                response,
                                path,
                                params,
                                textBody,
                                body
                            }
                        );
                    }
                    catch(error){
                        let isCustomResponse;
                        if(typeof onRouteError === 'function')
                            isCustomResponse = onRouteError(error, response);
                        else if(typeof onServerError === 'function')
                            isCustomResponse = onServerError(error, response);
                        if(!isCustomResponse)
                            response.code(500);
                        return response;
                    }
                }
            });
        },
        createDirectoryRoute: ({
            routePath,
            directoryPath
        }) => server.route({
            method: 'GET',
            path: `${routePath}{file*}`,
            handler: async (
                {
                    params: {
                        file
                    }
                },
                h
            ) => await exists(`${directoryPath}/${file || ''}`)
                ? file
                    ? h.directory(directoryPath)
                    : h.file(`${directoryPath}/index.html`)
                : h.response().code(404)
        })
    };
};